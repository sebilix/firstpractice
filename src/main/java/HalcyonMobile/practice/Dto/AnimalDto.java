package HalcyonMobile.practice.Dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AnimalDto {
    private int id;
    private String name;
    private int age;
    private String type;
}
