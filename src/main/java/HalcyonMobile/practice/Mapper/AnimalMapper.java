package HalcyonMobile.practice.Mapper;

import HalcyonMobile.practice.Dto.AnimalDto;
import HalcyonMobile.practice.Entity.Animal;
import org.springframework.stereotype.Component;

@Component
public class AnimalMapper {
    public Animal dtoToModel(final AnimalDto animalDto) {
        return Animal.builder()
                .name(animalDto.getName())
                .age(animalDto.getAge())
                .type(animalDto.getType())
                .build();
    }

    public AnimalDto modelToDto(final Animal animal) {
        return AnimalDto.builder()
                .id(animal.getId())
                .name(animal.getName())
                .age(animal.getAge())
                .type(animal.getType())
                .build();
    }
}
