package HalcyonMobile.practice.Repo;

import HalcyonMobile.practice.Entity.Animal;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnimalRepo extends JpaRepository<Animal, Integer> {
}
