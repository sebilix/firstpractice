package HalcyonMobile.practice.Service;

import HalcyonMobile.practice.Dto.AnimalDto;
import HalcyonMobile.practice.Entity.Animal;
import HalcyonMobile.practice.Mapper.AnimalMapper;
import HalcyonMobile.practice.Repo.AnimalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnimalService {
    @Autowired
    private AnimalRepo animalRepo;
    @Autowired
    private AnimalMapper animalMapper;

    public List<AnimalDto> getAnimals() {
        return animalRepo.findAll()
                .stream()
                .map(e -> animalMapper.modelToDto(e))
                .collect(Collectors.toList());
    }

    public AnimalDto getOne(final int id) {
        return animalMapper.modelToDto(animalRepo.getOne(id));
    }

    public void save(final Animal animal) {
        animalRepo.save(animal);
    }

    public AnimalDto delete(final int id) {
        final AnimalDto deleted = animalMapper.modelToDto(animalRepo.getOne(id));
        animalRepo.deleteById(id);
        return deleted;
    }


}
